# puretoml

**This is still in early development, a lot may change from version to version!**

A pure Lua [TOML](https://toml.io/en/) parser and serializer written in [Yuescript](https://github.com/pigpigyyy/yuescript).
Only supports TOML 0.1.0, but support for later versions is being worked on.

## Installation

#### luarocks
Simply run:
```
luarocks install puretoml
```

#### Source
Run:
```
git clone
```

cd into the directory then run:
```
luarocks make
```
## Examples
#### Decoding a file
```lua
local toml = require("puretoml")

stuff = toml.decode_file("config.toml")
...
```
#### Decoding a string
```lua
local toml = require("puretoml")

local config_toml = [[
    debug = true

    [core]
    use_opengl = true
]]

config = toml.decode(config_toml)
...
```
#### Decoding dates
```lua
local toml = require("puretoml")

local time_toml = "time = 1979-05-27T07:32:00Z"

local time = toml.decode(time_toml) -- toml.decode(time_toml, {DateFormat = 1}) also works
print(time.time)
-- prints: 1979-05-27T07:32:00Z

time = toml.decode(time_toml, {DateFormat = 2})
print(time.time)
-- prints: 296652720
-- see https://www.lua.org/manual/5.4/manual.html#pdf-os.time

time = toml.decode(time_toml, {DateFormat = 3})
print(time.time)
-- prints: Sun May 27 07:32:00 1979
-- see https://www.lua.org/manual/5.4/manual.html#pdf-os.date
```
#### Encoding
```lua
local toml = require("puretoml")

local table_to_encode = {
    a = 2,
    b = {
        c = "hello",
        d = {"this", "is", "an", "array"}
    }
}

toml.encode(table_to_encode, "stuff.toml")
```
#### Encoding dates
```lua
local toml = require("puretoml")

local date_to_encode = {
    day = toml.encode_date(296652720, toml.TimeDateFormat)
}

toml.encode(date_to_encode, "day.toml")
-- day = 1979-05-27T07:32:00Z

date_to_encode.day = toml.encode_date("Sun May 27 07:32:00 1979", toml.DayDateFormat)

toml.encode(date_to_encode, "day2.toml")
-- day = 1979-05-27T07:32:00Z
```
